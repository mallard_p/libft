/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmallard <pmallard@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/06 19:08:13 by pmallard          #+#    #+#             */
/*   Updated: 2022/11/09 18:10:14 by pmallard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdint.h>

void	*ft_calloc(size_t nmenb, size_t size)
{
	void	*ptr;

	if (size != 0 && nmenb > SIZE_MAX / size)
		return (NULL);
	ptr = malloc(nmenb * size);
	if (!ptr)
		return (NULL);
	ft_bzero(ptr, size * nmenb);
	return (ptr);
}
