/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmallard <pmallard@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/22 12:13:57 by pmallard          #+#    #+#             */
/*   Updated: 2022/11/22 16:20:00 by pmallard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	**bfree(char **tab)
{
	int	i;

	i = 0;
	while (tab[i])
	{
		free(tab[i]);
		i++;
	}
	free (tab);
	return (NULL);
}

static int	word_len(char *str, char charset)
{
	int	i;

	i = 0;
	while (str[i] && str[i] != charset)
		i++;
	return (i);
}

static int	word_count(char *str, char charset)
{
	int	i;
	int	count;

	i = 0;
	count = 0;
	while (*str)
	{
		while (*str && *str == charset)
			str++;
		i = word_len(str, charset);
		str += i;
		if (i)
			count++;
	}
	return (count);
}

static char	*copy(char *str, int n)
{
	char	*res;

	res = malloc(sizeof(char) * (n + 1));
	if (!res)
		return (NULL);
	res[n] = '\0';
	while (n--)
		res[n] = str[n];
	return (res);
}

char	**ft_split(char *str, char charset)
{
	int		i;
	int		n;
	int		size;
	char	**tab;

	i = -1;
	if (!str)
		return (NULL);
	size = word_count(str, charset);
	tab = malloc(sizeof(char *) * (size + 1));
	if (!tab)
		return (NULL);
	while (++i < size)
	{
		while (*str && *str == charset)
			str++;
		n = word_len(str, charset);
		tab[i] = copy(str, n);
		if (!tab[i])
			return (bfree(tab));
		str += n;
	}
	tab[size] = 0;
	return (tab);
}
