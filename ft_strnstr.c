/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmallard <pmallard@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/08 23:51:02 by pmallard          #+#    #+#             */
/*   Updated: 2022/11/12 23:39:44 by pmallard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *s1, const char *s2, size_t n)
{
	size_t	i;
	size_t	j;

	i = 0;
	if (!*s2)
		return ((char *)s1);
	if (n == 0)
		return (NULL);
	while (s1[i])
	{
		j = 0;
		if (s1[i] == s2[j])
		{
			while (i + j < n && s1[i + j] && s1[i + j] == s2[j])
			{
				j++;
				if (!s2[j])
					return ((char *)&s1[i]);
			}
		}
		i++;
	}
	return (NULL);
}
