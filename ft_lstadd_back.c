/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstadd_back.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmallard <pmallard@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/07 19:37:00 by pmallard          #+#    #+#             */
/*   Updated: 2022/11/12 00:10:47 by pmallard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstadd_back(t_list **lst, t_list *new)
{
	t_list	*pos;

	pos = ft_lstlast(*lst);
	if (!lst || !new)
		return ;
	if (!*lst)
		*lst = new;
	else
		pos->next = new;
}
