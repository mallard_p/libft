/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmallard <pmallard@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/06 18:16:17 by pmallard          #+#    #+#             */
/*   Updated: 2022/11/09 17:42:14 by pmallard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dest, const char *src, size_t n)
{
	size_t	i;
	size_t	size;

	i = 0;
	size = 0;
	while (size < n && dest[size])
		size++;
	while (src[i] && n && size + i < n - 1)
	{
		dest[size + i] = src[i];
		i++;
	}
	if (size < n)
		dest[size + i] = '\0';
	i = ft_strlen(src);
	return (i + size);
}
