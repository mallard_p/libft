/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmallard <pmallard@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/22 16:32:43 by pmallard          #+#    #+#             */
/*   Updated: 2022/12/07 20:30:56 by pmallard         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_nbrlen(long nb)
{
	int	i;

	i = 0 + (nb == 0);
	if (nb < 0)
	{
		nb = -nb;
		i++;
	}
	while (nb)
	{
		nb /= 10;
		i++;
	}
	return (i);
}

char	*ft_itoa(int nb)
{
	int		len;
	long	n;
	char	*res;

	n = nb;
	len = ft_nbrlen(n);
	res = malloc(sizeof(char) * (len + 1));
	if (!res)
		return (0);
	if (n < 0)
	{
		n = -n;
		res[0] = '-';
	}
	res[len--] = '\0';
	if (n == 0)
		res[0] = '0';
	while (n > 0)
	{
		res[len] = n % 10 + '0';
		n /= 10;
		len--;
	}
	return (res);
}
